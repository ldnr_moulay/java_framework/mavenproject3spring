package fr.ldnr.kevin.mavenproject3spring;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class EspaceMain {

	public static void main(String[] args) {
		System.out.println("***Voyages dans l'espace***");
		AbstractXmlApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:springcontext.xml");
		// arrêt du context lorsque l'application s'arrête
		ctx.registerShutdownHook();

		Distance dSoleil = ctx.getBean("distanceSoleil", Distance.class);
		dSoleil.setUnitesAstronomiques(1);
		System.out.println("Nous sommes à " + dSoleil.getKm() + " km du soleil");

		Distance dAL = ctx.getBean("distanceAnneeLumiere", Distance.class);
		System.out.println("1 AL =  " + dAL.getKm() + " km ");

		Distance dTSoleil = ctx.getBean("allerRetourTerreSoleil", Distance.class);
		System.out.println("1 aller retour terre soleil =  " + dTSoleil.getParsecs() * 2 + "parsecs");

		Distance ARToulouseParis = ctx.getBean("allerRetourToulouseParis", Distance.class);
		System.out.println("aller retour toulouse paris =  " + ARToulouseParis.getAnneesLumiere() * 2 + "AL");

		// ON utilise la classe Integer et on modifie l'attribut dans son constructeur
		Integer nbPLanetes = ctx.getBean("nbPLanetes", Integer.class);
		System.out.println("IL y a " + nbPLanetes + " planetes dans notre systeme solaire");

		// ON utilise la classe String et on modifie l'attribut dans son constructeur
		String galaxiNom = ctx.getBean("galaxiNom", String.class);
		System.out.println("Notre galaxie d'appelle " + galaxiNom);

		Calendar depart = new GregorianCalendar(2021, 9, 3);
		Calendar maintenant = ctx.getBean("dateMaintenant", GregorianCalendar.class);
		long diffMillis = depart.getTimeInMillis() - maintenant.getTimeInMillis();
		System.out.println("prochain décollage dans " + (diffMillis / (1000 * 3600)) + "h");

		Planete mars = ctx.getBean("mars", Planete.class);
		System.out.println("Mars a comme 1er satellite: " + mars.getSatellites().get(0));// 1000

		Distance d1 = ctx.getBean("distanceAnneeLumiere", Distance.class);
		System.out.println(String.format("d1 : %.3f  A.L.", d1.getAnneesLumiere()));
		d1.setAnneesLumiere(3);
		System.out.println(String.format("d1 : %.3f  A.L.", d1.getAnneesLumiere()));// 3000

		Distance d2 = ctx.getBean("distanceAnneeLumiere", Distance.class);
		System.out.println(String.format("d2 : %.3f  A.L.", d2.getAnneesLumiere()));// 3000

		Distance d3 = ctx.getBean("allerRetourTerreSoleil", Distance.class);
		d3.setUnitesAstronomiques(4);
		System.out.println(String.format("d3 : %.3f  U.A.", d3.getUnitesAstronomiques()));// 4000

		Distance d4 = ctx.getBean("allerRetourTerreSoleil", Distance.class);
		d4.setUnitesAstronomiques(4);
		System.out.println(String.format("d4 : %.3f  U.A.", d4.getUnitesAstronomiques()));// 2000

		Vaisseau v1 = ctx.getBean("pillarOfAutumn", Vaisseau.class);
		System.out.println("Vaisseau en partance pour " + v1.getDestination().getNom());
		
		

	}

}
