package fr.ldnr.kevin.mavenproject3spring;

public class Distance {
	private static final double PARSEC_EN_KM = 3.086e13;
	private static final double UA_EN_KM = 1.495e08;
	private static final double ANNEELUMIERE_EN_KM = 9.460e12;

	private double km;

	public Distance() {
		km = 0;
	}
	public Distance(double km) {
		this.km = km;
	}

	public double getKm() {
		return km;
	}

	public void setKm(double km) {
		this.km = km;
	}

	public double getParsecs() {
		return km / PARSEC_EN_KM;
	}

	public void setParsecs(double p) {
		km = p * PARSEC_EN_KM;
	}

	public double getUnitesAstronomiques() {
		return km / UA_EN_KM;
	}

	public void setUnitesAstronomiques(double ua) {
		km = ua * UA_EN_KM;
	}

	public double getAnneesLumiere() {
		return km / ANNEELUMIERE_EN_KM;
	}

	public void setAnneesLumiere(double al) {
		km = al * ANNEELUMIERE_EN_KM;
	}
	
	
	public void initialisee() {
		System.out.println("*Nouvelle distance : "+km+ " km");	
	}

}
