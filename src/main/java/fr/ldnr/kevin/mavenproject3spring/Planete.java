package fr.ldnr.kevin.mavenproject3spring;

import java.util.List;

public class Planete {
	private String nom;
	private List<String> satellites;

	public Planete() {
		nom = null;
		satellites = null;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<String> getSatellites() {
		return satellites;
	}

	public void setSatellites(List<String> satelites) {
		this.satellites = satelites;
	}

}
