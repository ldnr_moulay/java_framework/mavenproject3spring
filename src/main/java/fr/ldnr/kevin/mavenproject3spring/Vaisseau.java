package fr.ldnr.kevin.mavenproject3spring;

public class Vaisseau {
	private String nom;
	private double masse;
	private Planete destination;
	
	
	public Vaisseau() {
		nom=null;
		masse=0;
		destination=null;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public double getMasse() {
		return masse;
	}


	public void setMasse(double masse) {
		this.masse = masse;
	}


	public Planete getDestination() {
		return destination;
	}


	public void setDestination(Planete destination) {
		this.destination = destination;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
